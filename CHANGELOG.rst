.. _changelog:

Changelog
=========

0.0.1 (2015-06-14)
------------------

Initial protoype.

* Provides hex2words function for use as a library from external apps
* Parses sha1sum output and similar commands (sha512sum, etc.)
* Parses command-line arguments
* Parses *GPG --list-keys* output


# coding: utf-8


from hex2words.input import (
    process_XXXsum_output_line, process_GPG_output_line,
    process_input,
)


# Tests for process_XXXsum_output_line():
def test_XXXsum_1():
    input = '4ae9441337aba65ec89304f5afd87fbd  foobar.txt'
    expected_output = ('4ae9441337aba65ec89304f5afd87fbd', 'foobar.txt')
    real_output = process_XXXsum_output_line(input)
    real_output = (real_output[0], real_output[1])
    assert real_output == expected_output


# Tests for process_GPG_output_line():
def test_GPG_1():
    input = '   Key fingerprint = 6282 432A 74BE 3561 7FC3  B935 AED0 F18B F604 2B33'
    expected_output = '6282432A74BE35617FC3B935AED0F18BF6042B33'
    real_output = process_GPG_output_line(input)
    real_output = real_output
    assert real_output == expected_output

    
def test_GPG_2():
    input = 'Wr0Ng F0rm4t'
    expected_output = None
    real_output = process_GPG_output_line(input)
    assert real_output == expected_output


# Tests for process_input():
def test_process_input_1():
    input = (
        '4ae9441337aba65ec89304f5afd87fbd  foobar.txt\n',
        'd63f5b0ef39b05144a0716053d9a7f32  file45.pdf\n',
    )
    expected_output = '''foobar.txt:
\t4ae9441337aba65ec89304f5afd87fbd
\tdogsled ultimate crumpled barbecue clamshell Pegasus rematch finicky spaniel molasses adrift visitor rocker stupendous lockup quantity

file45.pdf:
\td63f5b0ef39b05144a0716053d9a7f32
\tstockman customer erase Atlantic upset Norwegian adult belowground dogsled amusement backward almighty commence newsletter lockup component
\n'''
    real_output = process_input(input)
    assert expected_output == real_output

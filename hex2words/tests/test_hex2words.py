# coding: utf-8


from hex2words.hex2words import hex2words


def test_1byte():
    input = '00'
    expected_output = 'aardvark'
    real_output = hex2words(input)
    assert expected_output == real_output

def test_2bytes():
    input = '0000'
    expected_output = 'aardvark adroitness'
    real_output = hex2words(input)
    assert expected_output == real_output
    
def test_3bytes():
    input = '000000'
    expected_output = 'aardvark adroitness aardvark'
    real_output = hex2words(input)
    assert expected_output == real_output

def test_vector1():
    input = 'E582'
    expected_output = 'topmost Istanbul'
    real_output = hex2words(input)
    assert expected_output == real_output
    
def test_vector1_reversed():
    input = '82E5'
    expected_output = 'miser travesty'
    real_output = hex2words(input)
    assert expected_output == real_output

def test_160bits():
    input = 'E58294F2E9A227486E8B061B31CC528FD7FA3F19'
    expected_output = 'topmost Istanbul Pluto vagabond '\
        'treadmill Pacific brackish dictator '\
        'goldfish Medusa afflict bravado '\
        'chatter revolver Dupont midsummer '\
        'stopwatch whimsical cowbell bottomless'
    real_output = hex2words(input)
    assert expected_output == real_output

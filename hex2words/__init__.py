# coding: utf-8


from .hex2words import hex2words

__all__ = (hex2words, )
